FROM node:8-alpine

RUN npm install

ENTRYPOINT ["node"]
CMD ["server.js"]